import os
import argparse

from aentsoe.attrdict import AttrDict


def get_args():
    """Process command line options

    :return: instance of ArgumentParser
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("config", action="store")
    parser.add_argument("-u", dest="update", action="store_true", default=False)
    args = parser.parse_args() # automatically looks at sys.argv
    return args


def get_data_config(filename):
    """Parse configuration options from file

    :param filename:
    :return: instance of AttrDict
    """
    msg = f"The config file '{filename}' does not exist"
    assert os.path.exists(filename), msg

    dconfig = AttrDict.from_yaml(filename)

    root_pth = os.path.abspath(os.path.dirname(filename))
    dconfig.root_pth = root_pth
    dconfig.data_pth = os.path.join(root_pth, dconfig.data_fldr)
    dconfig.dataset_pth = os.path.join(root_pth, dconfig.dataset_fldr)
    dconfig.pdataset_pth = os.path.join(root_pth, dconfig.pdataset_fldr)
    dconfig.stash_pth = os.path.join(root_pth, dconfig.stash_fldr)
    dconfig.download_pth = os.path.join(root_pth, dconfig.download_fldr)

    pths = [k for k in dconfig.keys() if "_pth" in k]
    for p in pths:
        os.makedirs(dconfig[p], exist_ok=True)

    hourlys = ["hourly_generation", "hourly_load"]
    for f in hourlys:
        pth = os.path.join(dconfig.stash_pth, f)
        os.makedirs(pth, exist_ok=True)
    for f in (hourlys + ["icap", "stored_energy"]):
        pth = os.path.join(dconfig.download_pth, f)
        os.makedirs(pth, exist_ok=True)

    try:
        td = dconfig.TIMEDOMAINS
        dconfig.TIMEDOMAINS = {k: tuple(map(str, v)) for k, v in td.items()}
    except KeyError:
        pass

    return dconfig
