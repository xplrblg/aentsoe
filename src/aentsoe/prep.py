"""References:
https://pandas.pydata.org/docs/user_guide/timeseries.html#offset-aliases

"""
import os.path
from datetime import datetime

import numpy as np
import pandas as pd
import dask.dataframe as dd

from aentsoe.config import get_args, get_data_config
from aentsoe.helpers import timeit, _data_maps, _data

rf_fileprefix = {"hourly": "h_rating_factor",
                 "monthly": "m_capacity_factor"}
profile_techs = {"monthly": ["Hydro turbine"],
                 "hourly": ["Other RES", "Hydro run", "Solar PV",
                            "Wind offshore", "Wind onshore"]}
date_format = {"monthly": "%Y-%m-%d", "hourly": "%Y-%m-%d %H:%M:%S"}


def get_ntc_lines_maf():
    """

    :return: dataframe
    """
    maf_file = os.path.join(data_pth, "MAF_2018_Dataset_V2.xlsx")
    maf_ntc = pd.read_excel(maf_file, sheet_name="NTCs", skiprows=[0])

    maf_lines_2025 = (maf_ntc
                      .iloc[:, [2]]
                      .rename(columns={"Border/boundary": "line"})
                      )

    maf_lines_2020 = (maf_ntc
                      .iloc[:, :2]
                      .rename(columns={"Border/boundary": "line"})
                      )

    all_lines = (pd.concat([maf_lines_2020,
                     maf_lines_2020.iloc[:, 0].str.split(pat = "-", expand=True)
                     .rename(columns={0: "From", 1: "To", 2: "To2"})], axis=1)
          .rename(columns={"Border/boundary": "line"})
          .assign(back=lambda x: x["To"].str.cat(x["From"], sep="-"),
                  line=lambda x: x["From"].str[:2].str.upper().str.cat(x["From"].str[2:]).str.cat(
                  x["To"].str[:2].str.upper().str.cat(x["To"].str[2:]), sep="-"))
         )

    split_lines = pd.concat([
    all_lines
    .loc[pd.notnull(all_lines["To2"])]
    .assign(From=lambda x: x["From"] + "00",
            To=lambda x: x["To"] + "00",
            To2=lambda x: x["To2"] + "00")
    .assign(line=lambda x: x["From"].str.cat(x["To"], sep="-"),
            back=lambda x: x["To"].str.cat(x["From"], sep="-")),
    all_lines
    .loc[pd.notnull(all_lines["To2"])]
    .assign(From=lambda x: x["From"] + "00",
            To=lambda x: x["To"] + "00",
            To2=lambda x: x["To2"] + "00")
        .assign(line=lambda x: x["To"].str.cat(x["To2"], sep="-"),
            back=lambda x: x["To2"].str.cat(x["To"], sep="-"))
    ])

    draft_lines = (pd.concat([all_lines
                             .loc[pd.isnull(all_lines["To2"])],
                             split_lines])
                   .drop(columns=["From", "To", "To2"])
                  )

    line = draft_lines["line"].values
    back = draft_lines["back"].values

    duplicates = []
    for i, l in enumerate(line):
        if back[i] in line[i:]:
            duplicates.append(back[i])

    ntc_obj = set(line) - set(duplicates)

    ntc_lines = (pd.merge(draft_lines.loc[draft_lines["line"].isin(list(ntc_obj))],
                         draft_lines, how="left", left_on="back", right_on="line", suffixes=("","_back"))
                 .drop(columns=["back", "line_back", "back_back"])
                 .rename(columns={"NTC (MW)": "Max Flow","NTC (MW)_back": "Min_Flow"})
                 .assign(Min_Flow=lambda x: -x["Min_Flow"])
                 .rename(columns={"Min_Flow": "Min Flow"})
                )

    return ntc_lines


def get_ntc_lines_ty18(ntc_file):
    """

    :param ntc_file:
    :return: dataframe
    """
    # ty18_ntc_file = os.path.join(data_pth, "TY18_NTC.csv")
    # ty18_ntc_scenarios = pd.read_csv(ty18_ntc_file, index_col=[0],
    # ty18_ntc_scenarios = pd.read_csv(ntc_file, index_col=[0], header=[0, 1])
    # lines = ty18_ntc_scenarios["NTC 2027"]
    lines = (pd.read_csv(ntc_file, index_col=0, header=[0, 1])
             .stack(level=[0, 1])
             .reset_index(drop=False)
             .assign(unit="MW")
             .rename(columns={"level_0": "line","level_1": "case","level_2": "property",0: "value"}).assign(unit="MW")
             .loc[:, ["line", "case", "property","value", "unit"]]
    )
    return lines


def get_line_node(lines):
    """

    :param lines:
    :return: dataframe
    """
    # lines.index.rename("line", inplace=True)
    # lines = pd.DataFrame({"line": lines.index.to_numpy()})
    lines = pd.DataFrame({"line": lines["line"].to_numpy()})
    lines_nodes = pd.concat([lines,
                             lines["line"].str.split(pat="-", expand=True)
                            .rename(columns={0: "node from", 1: "node to"})],
                            axis=1)
    Line_Node_From = lines_nodes.loc[:, ["line", "node from"]].rename(
        columns={"node from": "node"})
    Line_Node_To = lines_nodes.loc[:, ["line", "node to"]].rename(
        columns={"node to": "node"})
    return Line_Node_From, Line_Node_To


def get_node_region(lines):
    """

    :param lines:
    :return: dataframe
    """
    Node_Region = (pd.DataFrame(np.unique(np.hstack(
        # lines.index.str.split(pat="-", expand=True).to_numpy())),
        lines["line"].str.split(pat="-", expand=True).to_numpy())),
            columns=["Node"]).assign(Region=lambda x: x["Node"].str[:2]))
    return Node_Region


def get_load(input_file, scenarios):
    """

    :param input_file:
    :param scenarios:
    :return: dataframe
    """
    # ty18_input_file = os.path.join(data_pth, "TYNDP 2018 Input Data.xlsx")
    ty18_load_scenarios = pd.read_excel(input_file, sheet_name="Demand",
                                        index_col=[0], header=[0])
    ty18_load_scenarios.stack().head()
    idx = pd.IndexSlice
    # dfs = pd.DataFrame()
    # for scenario, timerefs in scenarios.items():
    #     dfs.append(pd.concat([(ty18_load_scenarios
    #            .loc[idx[:, [v]]]
    #            .assign(year=k, scenario=scenario)
    #            # .rename(columns={v: k})
    #            .rename(columns={v: "value"})
    #            ) for k, v in timerefs.items()]))

    return (pd.concat([pd.concat([(ty18_load_scenarios
                                   .loc[idx[:, [v]]]
                                   .assign(year=k, scenario=scenario)
                                   .rename(columns={v: "value"})
                                   ) for k, v in timerefs.items()])
                       for scenario, timerefs in scenarios.items()])
            .loc[:, ["year", "value", "scenario"]]
            .reset_index()
            .sort_values(by=["index", "scenario", "year"])
            .set_index("index"))


def get_icap_ty18(icap_file, scenarios):
    """

    :param icap_file:
    :param scenarios:
    :return: dataframe
    """
    # data_file = "ENTSO Scenario 2018 Generation Capacities.xlsx"
    # ty18_icap_file = os.path.join(data_pth, data_file)
    def get_icap_ty18_scen(scenario):
        ty18_icap_scen = pd.read_excel(icap_file, sheet_name=scenario,
                                       skiprows=[0, 1])
        ty18_icap_scen.columns = (ty18_icap_scen.columns
                                  .str.replace("\n", "")
                                  .str.replace("-", " ")
                                  .str.replace("on shore", "onshore")
                                  .str.replace("off shore", "offshore")
                                  .str.replace("Othernon RES", "Other nonRES")
                                  )
        ty18_icap_scen = (ty18_icap_scen
                          .rename(
            columns={"Country/Installed capacity (MW)": "zone"})
                          .set_index("zone")
                          .stack()
                          .rename("icap")
                          )
        ty18_icap_scen.index.set_names(["zone", "type"], inplace=True)

        ty18_icap_scen = (ty18_icap_scen
                              .reset_index()
                              .assign(
            generator=lambda x: x["zone"].str.cat(x["type"], sep="-"))
                              .loc[:, ["zone", "type", "generator", "icap"]]
                              )
        return ty18_icap_scen

    return pd.concat([pd.concat([get_icap_ty18_scen(v)
                                .assign(year=k, scenario=scenario)
                                 for k, v in timerefs.items()])
                      for scenario, timerefs in scenarios.items()])


def get_generator_node(gen_icap):
    """

    :param gen_icap:
    :return: dataframe
    """
    Generator_Node = (gen_icap
                      .loc[:, ["generator", "zone"]]
                      .rename(columns={"zone": "node"})
                      .drop_duplicates()
                      )
    return Generator_Node


def get_emission_fuel(type_fuel):
    """

    :param type_fuel:
    :return: dataframe
    """
    Emission_Fuel = type_fuel.loc[:, ["emission", "fuel"]].dropna(
        subset=["emission"])
    return Emission_Fuel


def get_generator_fuel(gen_icap, type_fuel):
    """

    :param gen_icap:
    :param type_fuel:
    :return: dataframe
    """
    Generator_Fuel = (pd.merge(gen_icap, type_fuel, how="left", on="type")
                      .loc[: , ["generator", "fuel"]]
                      .dropna(subset=["fuel"])
                      )
    return Generator_Fuel


def get_generator_storage(gen_icap):
    """

    :param gen_icap:
    :return: tuple of dataframes
    """
    hydros = ["Hydro turbine", "Hydro pump"]
    Generator_Storage_Head = (gen_icap
                                  .loc[:, ["type", "generator"]]
                                  .loc[gen_icap["type"].isin(hydros)]
                                  .drop_duplicates()
                                  .assign(
        storage=lambda x: x.generator + "-head")
                                  .loc[:, ["generator", "storage"]]
    )
    Generator_Storage_Tail = (gen_icap
                                  .loc[:, ["type", "generator"]]
                                  .loc[gen_icap["type"].isin(["Hydro pump"])]
                                  .drop_duplicates()
                                  .assign(
        storage=lambda x: x.generator + "-tail")
                                  .loc[:, ["generator", "storage"]]
    )
    return Generator_Storage_Head, Generator_Storage_Tail


def get_generator_data(gen_props_map, ty18_icap_scen, type_fuel):
    """

    :param gen_props_map:
    :param ty18_icap_scen:
    :param type_fuel:
    :return: dataframe
    """
    gen_properties_map = dict(zip(gen_props_map.label, gen_props_map.property))

    ty18_unit_file = os.path.join(data_pth, "TY18_Unit_Data.csv")
    ty18_unit = (pd.read_csv(ty18_unit_file, encoding="windows-1252",
                             header=[1])  # , index_col=[0,1])
                 .rename(columns=gen_properties_map))

    ty18_unit.loc[ty18_unit["Fuel"] == "Nuclear", ["Type"]] = "Nuclear"
    ty18_unit.loc[
        (ty18_unit["Fuel"] == "Hard coal") & (ty18_unit["Type"] == "New"), [
            "Type"]] = "Hard coal new"
    ty18_unit.loc[
        (ty18_unit["Fuel"] == "Lignite") & (ty18_unit["Type"] == "New"), [
            "Type"]] = "Lignite new"
    ty18_unit.loc[ty18_unit["Fuel"] == "Light oil", ["Type"]] = "Oil"
    ty18_unit.loc[
        (ty18_unit["Fuel"] == "Oil shale") & (ty18_unit["Type"] == "new"), [
            "Type"]] = "Oil shale"

    gen_props = pd.merge(ty18_icap_scen,
                         pd.merge(type_fuel, ty18_unit, how="right",
                                  left_on=["Type"], right_on=["Type"]).dropna(
                             subset=["type", "fuel"]),
                         how="left", on="type").dropna(how="any")
    return gen_props


def get_unit_data(dataconfig):
    """

    :param dataconfig:
    :return: dataframe
    """
    filename = dataconfig.generator_properties_file
    gen_props_map_file = _data_maps(filename)
    gen_props_map = pd.read_csv(gen_props_map_file, header=None,
                                names=["label", "property"])
    gen_properties_map = dict(zip(gen_props_map.label, gen_props_map.property))

    filename = dataconfig.references[0].unit_file
    ty18_unit_file = os.path.join(dataconfig.data_pth, filename)
    return (pd.read_csv(ty18_unit_file, encoding="windows-1252", header=[1])
            .rename(columns=gen_properties_map)
            .assign(Generator_HeatRate= lambda x: 3.6 / x.Generator_HeatRate))


def get_generator_attrs(dataconfig, type_fuel):
    """

    :param dataconfig:
    :param type_fuel:
    :return: dataframe
    """
    ty18_unit = get_unit_data(dataconfig)

    ty18_unit.loc[ty18_unit["Fuel"] == "Nuclear", ["Type"]] = "Nuclear"
    ty18_unit.loc[
        (ty18_unit["Fuel"] == "Hard coal") & (ty18_unit["Type"] == "New"), [
            "Type"]] = "Hard coal new"
    ty18_unit.loc[
        (ty18_unit["Fuel"] == "Lignite") & (ty18_unit["Type"] == "New"), [
            "Type"]] = "Lignite new"
    ty18_unit.loc[ty18_unit["Fuel"] == "Light oil", ["Type"]] = "Oil"
    ty18_unit.loc[
        (ty18_unit["Fuel"] == "Oil shale") & (ty18_unit["Type"] == "new"), [
            "Type"]] = "Oil shale"

    return (pd.merge(type_fuel, ty18_unit, how="right",
                    left_on=["Type"], right_on=["Type"])
            .dropna(subset=["type", "fuel"]))


def get_commodities(commodities_file):
    """

    :param commodities_file:
    :return: dataframe
    """
    ty18_commodities = (pd.read_csv(commodities_file,
                                    encoding="windows-1252", header=[0, 1],
                                    index_col=[0, 1])
                            .stack(level=[0, 1])
                            .reset_index(drop=False)
                            .rename(columns={"level_0": "unit",
                                             "level_1": "commodity",
                                             "level_2": "year",
                                             "level_3": "case",
                                             0: "value"})
                            .loc[:, ["commodity", "year", "case",
                                     "value", "unit"]])
    return ty18_commodities

@timeit
def prep_dataset(dataconfig):
    """

    :param dataconfig:
    :return: None
    """
    data_pth = dataconfig.data_pth
    dataset_pth = dataconfig.dataset_pth
    os.makedirs(dataset_pth, exist_ok=True)
    pdataset_pth = dataconfig.pdataset_pth
    os.makedirs(pdataset_pth, exist_ok=True)
    pth = dataset_pth
    ppth = pdataset_pth

    # lines
    filename = dataconfig.references[0].ntc_file
    ty18_ntc_file = os.path.join(data_pth, filename)
    lines = get_ntc_lines_ty18(ty18_ntc_file)
    line_node_from, line_node_to = get_line_node(lines)
    node_region = get_node_region(lines)
    node_region.to_csv(os.path.join(ppth, "_Node_Region.csv"), index=False)

    lines.to_csv(os.path.join(pth, "line_max_min_flow.csv"), index=False)
    line_node_from.to_csv(os.path.join(ppth, "_Line_Node_From.csv"), index=False)
    line_node_to.to_csv(os.path.join(ppth, "_Line_Node_To.csv"), index=False)

    ty18_input_file = os.path.join(data_pth, "TYNDP 2018 Input Data.xlsx")
    raw_demscenarios = dataconfig.references[0].dem_scenarios
    demscenarios = {r.name: r.timerefs for r in raw_demscenarios}
    # scenario = "2030DG"
    # get_load(ty18_input_file, demscenarios).to_csv(os.path.join(pth, f"load_{scenario}.csv"),
    get_load(ty18_input_file, demscenarios).to_csv(os.path.join(pth, f"load.csv"),
                                     header=False)
    # scenario = "2030 DG"
    # gen_icap = get_icap_ty18(scenario)
    raw_genscenarios = dataconfig.references[0].gen_scenarios
    data_file = "ENTSO Scenario 2018 Generation Capacities.xlsx"
    ty18_icap_file = os.path.join(data_pth, data_file)
    genscenarios = {r.name: r.timerefs for r in raw_genscenarios}
    gen_icap = get_icap_ty18(ty18_icap_file, genscenarios)
    # scenario = scenario.replace(" ", "")
    # gen_icap.to_csv(os.path.join(pth, f"icap_{scenario}.csv"), header=False)
    # gen_icap.to_csv(os.path.join(pth, f"generator_scen_icap.csv"), index=False)
    gen_icap.to_csv(os.path.join(pth, f"generator_icap_scenarios.csv"), index=False)
    get_generator_node(gen_icap).to_csv(
        os.path.join(pth, "generator_node.csv"), index=False)

    type_fuel = pd.read_csv(_data_maps(dataconfig.type_fuel_file))

    get_emission_fuel(type_fuel).to_csv(os.path.join(ppth,"_Emission_Fuel.csv"),
                                        index=False)
    get_generator_fuel(gen_icap, type_fuel).to_csv(
        os.path.join(ppth, "_Generator_Fuel.csv"), index=False)
    generator_storage_head, generator_storage_tail = get_generator_storage(
        gen_icap)
    generator_storage_head.to_csv(
        os.path.join(ppth, "_Generator_Storage_Head.csv"), index=False)
    generator_storage_tail.to_csv(
        os.path.join(ppth, "_Generator_Storage_Tail.csv"), index=False)

    gen_props_map_file = _data_maps(dataconfig.generator_properties_file)
    gen_props_map = pd.read_csv(gen_props_map_file, header=None,
                                names=["label", "property"])

    # gen_props = get_generator_data(gen_props_map, gen_icap, type_fuel)
    gen_attrs = get_generator_attrs(dataconfig, type_fuel)
    gen_attrs.to_csv(
        os.path.join(pth, "generator_data.csv"), index=False)

    # commodities
    filename = dataconfig.references[0].commodities_file
    ty18_commodities_file = os.path.join(data_pth, filename)
    get_commodities(ty18_commodities_file).to_csv(
        os.path.join(pth, "commodities.csv"), index=False,
        encoding="windows-1252")


def get_comm_prices(comm_data, comm_scenarios):
    """

    :param comm_data:
    :param comm_scenarios:
    :return: dataframe
    """
    comm_prices = (pd.concat([pd.concat([comm_data
                                       .loc[(comm_data["year"] == int(k)) &
                                            (comm_data["case"] == v)]
                                        for k, v in timerefs.items()])
                                       .assign(scenario=scenario)
                              for scenario, timerefs in comm_scenarios.items()])
                       .rename(columns={"commodity": "name"})
                       .assign(name=lambda x: x.name.str.replace(" price", ""))
                       .loc[:, ["name", "year", "value", "unit", "scenario"]])
    return comm_prices


def get_ntc_flows(prop, ntc_data, ntc_scenarios):
    """

    :param prop:
    :param ntc_data:
    :param ntc_scenarios:
    :return: dataframe
    """
    ntc_flows = (pd.concat([pd.concat([ntc_data
                                        # .loc[(ntc_data["property"] == "Max Flow") & (ntc_data["year"] == int(k)) &
                                        .loc[(ntc_data["property"] == prop) &
                                             (ntc_data["case"] == v)]
                                        .assign(year=int(k))
                             for k, v in timerefs.items()]).assign(scenario=scenario)
                              for scenario, timerefs in ntc_scenarios.items()])
                       .rename(columns={"line": "name"})
                       .loc[:, ["name", "year", "value", "unit", "scenario"]]
    )
    if prop == "Min Flow":
        ntc_flows["value"] = - ntc_flows["value"]
    return ntc_flows


def write_scen_grouped(label, data, pth):
    """

    :param label:
    :param data:
    :param pth:
    :return: None
    """
    scen_grouped = data.groupby(["scenario"])
    for k, gr in scen_grouped:
        filename = f"{label}_{k}.csv"
        gr.to_csv(os.path.join(pth, filename), index=False,
                  encoding="windows-1252")


def write_field_grouped(label, field, data, pth):
    """

    :param label:
    :param field:
    :param data:
    :param pth:
    :return: None
    """
    grouped = data.groupby([field])
    for k, gr in grouped:
        filename = f"{label}_{k}.csv"
        gr.to_csv(os.path.join(pth, filename), index=False,
                  encoding="windows-1252")

@timeit
def write_tech_grouped(prefix, suffix, data, pth):
    """

    :param prefix:
    :param suffix:
    :param data:
    :param pth:
    :return: None
    """
    tech_grouped = data.groupby(["technology"])
    for k, gr in tech_grouped:
        filename = f"{prefix}_{k}_{suffix}.csv"
        (gr
         .drop(columns=["technology"])
         .to_csv(os.path.join(pth, filename), index=False, encoding="windows-1252")
         )


def write_attrs(data, pth):
    """

    :param data:
    :param pth:
    :return: None
    """
    cols = data.columns
    for i, col in enumerate(cols):
        filename = os.path.join(pth, f"{col}.csv")
        (data.iloc[:, [i]]
         .rename(columns={col: "value"})
         .to_csv(filename))


def get_techs(data_scen):
    """

    :param data_scen:
    :return: list
    """
    return data_scen.scenario.drop_duplicates().to_list()


def get_gen_maxcap(data_actual, data_scen):
    """

    :param data_actual:
    :param data_scen:
    :return: list
    """
    scens = data_scen.scenario.drop_duplicates().to_list()

    actual = pd.concat([data_actual.assign(scenario=s) for s in scens])
    return (pd.concat([actual, data_scen])
                .assign(unit="MW")
                .loc[: , ["year", "name", "value", "unit", "scenario"]]
)


def pget_gen_emi_attrs(pth):
    """

    :param pth:
    :return: tuple of dataframes
    """
    filename = _data_maps("generator_properties.csv")
    attrs = pd.read_csv(filename, header=None).iloc[:, 1].tolist()
    gen_attrs = [a for a in attrs if a.startswith("Generator_")]
    emi_attrs = [a for a in attrs if a.startswith("Emission_")]
    filename = os.path.join(pth, "generator_icap_scenarios.csv")
    gens = (pd.read_csv(filename, header=0)
            .loc[:, ["generator", "type"]]
            .drop_duplicates())
    data = pd.read_csv(os.path.join(pth, "generator_data.csv"))
    all_data = pd.merge(gens, data, how="left", on="type")
    gdata = (all_data
             .rename(columns={"generator": "name"})
             .assign(pattern="H1-24")
             .set_index(["name", "pattern"])
             .loc[:, gen_attrs]
             .assign(Generator_MaintenanceRate=lambda x:
                    round(x.Generator_MaintenanceRate * 100 / 365, 1))
             .fillna(0))
    edata = (all_data
             .dropna(subset=["emission"])
             .assign(name=lambda x: x.emission.str.cat(x.fuel),
                     pattern="H1-24")
             .drop(columns=["Fuel"])
             .set_index(["name", "pattern"])
             .loc[:, emi_attrs]
             .drop_duplicates())
    return gdata, edata


def get_gen_properties(gen_props, pth):
    """

    :param gen_props:
    :param pth:
    :return:
    """
    map_gen_props = pd.read_csv(_data_maps("generator_properties.csv"),
                                header=None)
    props = [s.split(".") for s in dict(zip(map_gen_props[0],
                                            map_gen_props[1])).values()]
    parent, child, prop = zip(*props)
    return parent

@timeit
def prep1_pdataset(dataconfig):
    """

    :param dataconfig:
    :return: None
    """
    dataset_pth = dataconfig.dataset_pth
    pdataset_pth = dataconfig.pdataset_pth
    os.makedirs(pdataset_pth, exist_ok=True)

    # generator attributes
    gdata, edata = pget_gen_emi_attrs(dataset_pth)
    write_attrs(gdata, pdataset_pth)
    write_attrs(edata, pdataset_pth)

    # generator
    icap_cols = ["year", "generator", "icap"]
    colrenames = {"generator": "name", "icap": "value"}
    icap_actual = (pd.read_csv(os.path.join(dataset_pth,
                                            "generator_icap_actual.csv"))
                   .loc[:, icap_cols]
                   .rename(columns=colrenames)
                   )
    icap_scen = (pd.read_csv(os.path.join(dataset_pth,
                                          "generator_icap_scenarios.csv"))
                 .loc[:, icap_cols + ["scenario"]]
                 .rename(columns=colrenames)
                 )
    gen_maxcap = get_gen_maxcap(icap_actual, icap_scen)
    write_scen_grouped("Generator_Max_Cap", gen_maxcap, pdataset_pth)

    # lines
    ntc_data = pd.read_csv(os.path.join(dataset_pth, "line_max_min_flow.csv"))

    l_ntcscenarios = dataconfig.references[0].ntc_scenarios
    ntc_scenarios = {r.name: r.timerefs for r in l_ntcscenarios}

    line_maxflow = get_ntc_flows("Max Flow",ntc_data, ntc_scenarios)
    line_minflow = get_ntc_flows("Min Flow",ntc_data, ntc_scenarios)

    write_scen_grouped("Line_Max_Flow", line_maxflow, pdataset_pth)
    write_scen_grouped("Line_Min_Flow", line_minflow, pdataset_pth)

    # commodities
    comm_data = pd.read_csv(os.path.join(dataset_pth, "commodities.csv"),
                            encoding="windows-1252")
    fuel_data = comm_data.loc[comm_data["commodity"] != "CO2 price"]
    emission_data = comm_data.loc[comm_data["commodity"] == "CO2 price"]

    l_commscenarios = dataconfig.references[0].comm_scenarios
    comm_scenarios = {r.name: r.timerefs for r in l_commscenarios}

    fuel_prices = get_comm_prices(fuel_data, comm_scenarios)
    emission_prices = get_comm_prices(emission_data, comm_scenarios)

    write_scen_grouped("Fuel_Price", fuel_prices, pdataset_pth)
    write_scen_grouped("Emission_Price", emission_prices, pdataset_pth)


def map_proxies(dconfig, year):
    """

    :param dconfig:
    :param year:
    :return: dictionary
    """
    pth = dconfig.stash_pth
    csvfile = f"m_capacity_factor_proxy_{year}.csv"
    # csvfile = f"h_rating_factor_proxy_{year}.csv"
    map_Generator_proxy = pd.read_csv(os.path.join(pth, csvfile))
    return dict(zip(map_Generator_proxy.generator, map_Generator_proxy.proxy))


def get_generators(data, techs=None, zone=False, built=False):
    """

    :param data:
    :param techs:
    :param zone:
    :param built:
    :return: dataframe
    """
    df = data[["name"]]
    if built:
        df = df.loc[data["value"] != 0, ["name"]]
    df = df.drop_duplicates()
    if techs:
        df["technology"] = df["name"].str.split(pat="-").str[-1]
        df = df.loc[df["technology"].isin(techs), ["name"]]
    if zone:
        df = map_node_techs(df)
    return df

@timeit
def stack_data(filepattern, filename=None, index="datetime", cols=None):
    """

    :param filepattern:
    :param filename:
    :param index:
    :param cols:
    :return: dataframe
    """
    ddf = dd.read_csv(filepattern, header=0)
    df = ddf.compute().set_index(index)
    if index.lower() == "datetime":
        df.index = pd.to_datetime(df.index, format="%Y-%m-%d %H:%M:%S")
    if cols:
        df = df.loc[:, cols]
    if filename:
        df.to_csv(filename, index=True)
    return df


def get_aggregate_data(data, offset, filename=None):
    """

    :param data:
    :param offset:
    :param filename:
    :return: dataframe
    """
    df = (data
          .groupby("name")
          .resample(offset).mean()
          )
    if filename:
        df.to_csv(filename, index=True)
    return df

@timeit
def extend_icap_actual(data, fill=True):
    """

    :param data:
    :param fill:
    :return: dataframe
    """
    df = (data
          .reset_index()
          .set_index(["datetime", "name"])
          .unstack(level=0)
          .stack(dropna=False)
          .reset_index()
          )
    # https://github.com/pandas-dev/pandas/issues/27688#issuecomment-554181089
    if fill:
        df.update(df.sort_values(["name", "datetime"]).groupby("name").ffill())
        df.update(df.sort_values(["name", "datetime"]).groupby("name").bfill())
    df = df.set_index("datetime")
    return df

@timeit
def get_yearly_icap(dataconfig, techs=None, years=None, filename=None):
    """

    :param dataconfig:
    :param techs:
    :param years:
    :param filename:
    :return: dataframe
    """
    dataset_pth = dataconfig.dataset_pth
    icap_cols = ["generator", "icap"]
    colrenames = {"generator": "name", "icap": "value"}
    icap_actual = (pd.read_csv(os.path.join(dataset_pth, "generator_icap_actual.csv"))
                   .groupby(["zone", "type", "generator", "year"]).sum()
                   .reset_index())
    crit1 = icap_actual["type"].isin(techs)
    crit2 = icap_actual["year"].isin(years)
    df = (icap_actual.loc[crit1 & crit2, :]
          .groupby(["zone", "type", "generator", "year"]).sum()
          .reset_index()
          .assign(month=1, day=1)
          .assign(dt=lambda x: pd.to_datetime(x[['year', 'month', 'day']]))
          .rename(columns={"dt": "datetime"})
          .set_index("datetime")
          .loc[:, icap_cols]
          .rename(columns=colrenames)
          )
    if filename:
        df.to_csv(filename, index=True)
    return df

@timeit
def get_icap(ydata, year, offset, filename=None):
    """

    :param ydata:
    :param year:
    :param offset:
    :param filename:
    :return: dataframe
    """
    df = (ydata
          .groupby(["name"])
          .resample(offset).asfreq()
          )
    if offset == "H":
        df = df.interpolate(method='linear')
    elif offset == "MS":
        df = df.fillna(method='pad')
    df = (df
          .drop(columns=["name"])
          .reset_index()
          .set_index("datetime")
          .loc[f"{year}"]
          )
    if filename:
        df.to_csv(filename, index=True)
    return df

@timeit
def get_load_factor(genfile, icapfile, freq, dateformat,
                    techs=None, filename=None, summary=None, clip=None):
    """

    :param genfile:
    :param icapfile:
    :param freq:
    :param dateformat:
    :param techs:
    :param filename:
    :param summary:
    :param clip:
    :return: dataframe
    """
    dateparser = lambda x: datetime.strptime(x, dateformat)
    dfgen = pd.read_csv(genfile, header=0,
                        parse_dates=["datetime"], date_parser=dateparser)
    dfgen = (dfgen[dfgen['name'].str.contains('|'.join(techs))]
             .set_index(["datetime", "name"])
             )
    dficap = (pd.read_csv(icapfile, header=0,
                          parse_dates=["datetime"], date_parser=dateparser)
              .set_index(["datetime", "name"])
             )
    df = (dficap.merge(dfgen, how="left", left_index=True, right_index=True,
                       suffixes=("_icap", "_gen"))
          .assign(value=lambda x: round(100 * x.value_gen / x.value_icap, 1))
          .fillna(value={"value": 0})
          .drop(columns=["value_icap", "value_gen"])
          )

    if clip:
        lower, upper = clip
        df["value"] = df["value"].clip(upper=upper)

    df_summary = df.groupby(["name"]).describe()
    print(df_summary)
    if summary:
        df_summary.index.name = None
        df_summary.to_csv(summary)

    if filename:
        df = df.reset_index()
        df = df.assign(month= lambda x: x["datetime"].dt.month)
        if freq.lower() == "monthly":
            df = df.assign(day=1,
                           period=1)
        elif freq.lower() == "hourly":
            df = df.assign(day=lambda x: x["datetime"].dt.day,
                           period=lambda x: x["datetime"].dt.hour + 1)
        df = (df
              .drop(columns=["datetime"])
              .set_index(["month", "day", "period", "name"])
              )
        df.to_csv(filename, index=True)

    return df_summary


def read_load_factor(filename, tech=False):
    """

    :param filename:
    :param tech:
    :return: dataframe
    """
    df = pd.read_csv(filename)
    if tech:
        df = df.assign(technology=lambda x: x.name.str.split(pat="-").str[-1])
    return df


def map_node_techs(genlist):
    """

    :param genlist:
    :return: dataframe
    """
    blacklist = ["IS00", "IL00", "GR03"]
    shortlist = genlist[~genlist['name'].str.contains('|'.join(blacklist))]
    parsed = (shortlist["name"].str.split(pat="-", expand=True)
              .rename(columns={0:"node", 1:"tech"})
              .assign(cty=lambda x: x["node"].str[:2],
                      cty_tech=lambda x: x["cty"].str.cat(x["tech"], sep="-")))
    cols = ["name", "cty_tech"]
    return pd.concat([shortlist, parsed], axis=1).loc[:, cols]

@timeit
def get_proxy_load_factor(dconfig, genbuilt, genall, freq, year, filename=None):
    """

    :param dconfig:
    :param genbuilt:
    :param genall:
    :param freq:
    :param year:
    :param filename:
    :return: dataframe
    """
    proxies = map_proxies(dconfig, year)

    stash_pth = dconfig.stash_pth
    rf_filename = f"{rf_fileprefix[freq]}_core_{year}.csv"
    rating_file = os.path.join(stash_pth, rf_filename)
    rating_df = pd.read_csv(rating_file)

    alltechs = genbuilt.assign(proxy=lambda x: x["name"].map(proxies))
    alltechs["proxy"] = alltechs["proxy"].fillna(alltechs["cty_tech"])

    cres = (alltechs
            .merge(rating_df, how="left", left_on="proxy", right_on="name")
            .drop(columns=["cty_tech", "proxy", "name_y"])
            .rename(columns={"name_x": "name"})
            .dropna()  # res[pd.isnull(res).any(axis=1)]
            .astype({"month": "int32", "day": "int32", "period": "int32"})
            )
    genrating = cres[["name", "value"]].drop_duplicates()
    all_built_join = genall.merge(
        genrating, how="left", left_on="name", right_on="name")
    gennan = (all_built_join
              .loc[pd.isnull(all_built_join).any(axis=1), ["name"]]
              .assign(month=1, day=1, period=1, value=0)
              )
    res = pd.concat([cres, gennan], axis=0)
    if filename:
        res.to_csv(filename, index=False)
    return res


def get_proxy_tech(dconfig, year, ltechs, freq, filename=None):
    """

    :param dconfig:
    :param year:
    :param ltechs:
    :param freq:
    :param filename:
    :return: dataframe
    """
    pdataset_pth = dconfig.pdataset_pth
    stash_pth = dconfig.stash_pth
    techs = pd.DataFrame(ltechs)
    proxy_matrix_file = _data("proxy_matrix.csv")
    proxies = (pd.read_csv(proxy_matrix_file, index_col=0)
                .stack()
                .reset_index()
                .rename(columns={"level_1": "proxy", 0: "preference"})
                )

    ctys = proxies.loc[:, ["name", "proxy"]]
    ctys['_tmpkey'] = 1
    techs['_tmpkey'] = 1

    universe = pd.merge(ctys, techs, on='_tmpkey').drop('_tmpkey', axis=1)
    universe.index = pd.MultiIndex.from_product((ctys.index, techs.index))

    ctys.drop('_tmpkey', axis=1, inplace=True)
    techs.drop('_tmpkey', axis=1, inplace=True)

    universe = universe.rename(columns={0: "tech"})
    universe["name_tech"] = universe["name"].str.cat(universe["tech"], sep="-")
    universe["proxy_tech"]= universe["proxy"].str.cat(universe["tech"], sep="-")

    ranking = (universe
                   .merge(proxies,
                          left_on=["name", "proxy"], right_on=["name", "proxy"])
                   .loc[:,["name","tech","name_tech","proxy_tech","preference"]]
    )

    rf_filename = f"{rf_fileprefix[freq]}_core_{year}.csv"
    rating_factor = pd.read_csv(os.path.join(stash_pth, rf_filename))
    sum_rating_factor = (rating_factor
                         .loc[:, ["name", "value"]]
                         .groupby(["name"]).sum()
                         )
    avail = (sum_rating_factor[sum_rating_factor["value"] > 0]
             .reset_index()
             .loc[:,["name"]]
             .rename(columns={"name": "proxy_tech"})
             )

    res = (ranking
                .merge(avail,
                       left_on=["proxy_tech"], right_on=["proxy_tech"],
                       how="right")
                .sort_values(by=["name_tech", "preference"])
                .drop_duplicates()
                .drop_duplicates(subset=["name_tech"])
                .loc[:,["name", "tech", "name_tech", "proxy_tech"]]
    )

    node_region_file =  os.path.join(pdataset_pth, "_Node_Region.csv")
    nodereg = pd.read_csv(node_region_file)
    fres = (pd.merge(res, nodereg, left_on="name", right_on="Region",
                    how="right")
            .assign(name_tech=lambda x: x["Node"].str.cat(x["tech"], sep="-"))
            .loc[:, ["name_tech", "proxy_tech"]]
            .rename(columns={"name_tech": "generator", "proxy_tech": "proxy"})
            )

    if filename:
        fres.to_csv(filename, index=False)
    return fres


def get_freq_gen(dconfig, freqs=("hourly", "monthly")):
    """

    :param freqs:
    :return: None
    """
    pth = dconfig.stash_pth
    freqs = [f.lower() for f in freqs]
    cols = ["name", "value"]
    for y in range(2015, 2020):
        if "hourly" in freqs:
            pattern = os.path.join(pth, f"hourly_generation/10*_{y}_*.csv")
            h_destination = os.path.join(pth, f"h_generation_{y}.csv")
            h_gen = stack_data(pattern, h_destination, cols=cols)
        if "monthly" in freqs:
            m_destination = os.path.join(pth, f"m_generation_{y}.csv")
            get_aggregate_data(h_gen, "MS", filename=m_destination)


def get_freq_icap(dconfig):
    """

    :return: None
    """
    pth = dconfig.stash_pth
    offsets = {"hourly": "H", "monthly": "MS"}
    fprefixes = {"hourly": "h_icap", "monthly": "m_icap"}
    for freq in ["hourly", "monthly"]:
        techs = profile_techs[freq]
        offset = offsets[freq]
        fprefix = fprefixes[freq]
        for y in range(2015, 2020):
            y_icap = get_yearly_icap(dconfig, techs, years=[y, y + 1])
            y_icap = extend_icap_actual(y_icap)
            freq_icap_file = os.path.join(pth, f"{fprefix}_{y}.csv")
            get_icap(y_icap, y, offset, freq_icap_file)

@timeit
def get_freq_core_load_factor(dconfig):
    """

    :return: None
    """
    stash_pth = dconfig.stash_pth
    for freq in ["hourly", "monthly"]:
        use_profile_techs = profile_techs[freq]
        use_date_format = date_format[freq]
        for y in range(2015, 2020):
            genfile = os.path.join(stash_pth, f"{freq[0]}_generation_{y}.csv")
            icapfile = os.path.join(stash_pth, f"{freq[0]}_icap_{y}.csv")
            rf_fname = f"{rf_fileprefix[freq]}_core_{y}.csv"
            rf_summary_fname = f"{rf_fileprefix[freq]}_core_summary_{y}.csv"
            rating_file = os.path.join(stash_pth, rf_fname)
            rating_summary_file = os.path.join(stash_pth, rf_summary_fname)
            df_summary = get_load_factor(genfile, icapfile, freq,
                                         use_date_format, use_profile_techs,
                                         rating_file, rating_summary_file,
                                         clip=(None, 100.0))

@timeit
def get_freq_proxy_load_factor(dconfig):
    """

    :param dconfig:
    :return: None
    """
    pdataset_pth = dconfig.pdataset_pth
    stash_pth = dconfig.stash_pth
    # get hourly and monthly load factor
    for freq in ["hourly", "monthly"]:
        techs = profile_techs[freq]
        for y in range(2015, 2020):
            fname = f"{rf_fileprefix[freq]}_proxy_{y}.csv"
            filename = os.path.join(stash_pth, fname)
            get_proxy_tech(dconfig, y, techs, freq, filename)

    dataset_pth = dconfig.dataset_pth
    icap_cols = ["year", "generator", "icap"]
    colrenames = {"generator": "name", "icap": "value"}
    icap_actual = (pd.read_csv(os.path.join(dataset_pth, "generator_icap_actual.csv"))
                   .loc[:, icap_cols]
                   .rename(columns=colrenames)
                   )
    icap_scen = (pd.read_csv(os.path.join(dataset_pth, "generator_icap_scenarios.csv"))
                 .loc[:, icap_cols + ["scenario"]]
                 .rename(columns=colrenames)
                 )

    for freq in ["hourly", "monthly"]:
        gen_built = get_generators(icap_scen, profile_techs[freq], zone=True,
                                   built=True)
        gen_all = get_generators(icap_scen, profile_techs[freq], zone=True)
        for y in range(2015, 2020):
            fname = f"{rf_fileprefix[freq]}_{y}.csv"
            proxy_rating_file = os.path.join(stash_pth, fname)
            get_proxy_load_factor(dconfig, gen_built, gen_all, freq, y,
                                  filename=proxy_rating_file)

            pth = pdataset_pth
            df = read_load_factor(proxy_rating_file, tech=True)
            if freq == "hourly":
                write_tech_grouped("Generator_RatingFactor", y, df, pth)
            elif freq == "monthly":
                write_tech_grouped("Generator_CapacityFactorMonth", y, df, pth)

@timeit
def prep2_pdataset(dataconfig):
    """"""
    get_freq_gen(dataconfig)
    get_freq_icap(dataconfig)
    get_freq_core_load_factor(dataconfig)
    get_freq_proxy_load_factor(dataconfig)

@timeit
def main(dconfig):
    prep_dataset(dconfig)
    prep1_pdataset(dconfig)
    prep2_pdataset(dconfig)


if __name__ == "__main__":
    args = get_args()
    dconfig = get_data_config(args.config)
    main(dconfig)
