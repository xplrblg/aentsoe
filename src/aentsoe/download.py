"""https://transparency.entsoe.eu/content/static_content/Static%20content/
web%20api/Guide.html

Note: For obtaining a security token refer to section 2 of the
RESTful API documentation of the ENTSOE-E Transparency platform
https://transparency.entsoe.eu/content/static_content/Static%20content/
web%20api/Guide.html#_authentication_and_authorisation. Please save the
token in your config.yaml file (key 'entsoe_token').

"""
import os.path
import logging
import re
import glob
from io import StringIO

import requests
from dotenv import load_dotenv, find_dotenv

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

import dask.dataframe as dd
import pandas as pd

from aentsoe.config import get_args, get_data_config

from aentsoe.helpers import _data, timeit, _data_maps

logger = logging.getLogger(__name__)

techp = {'A03': 'Mixed',
         'A04': 'Generation',
         'A05': 'Load',
         'B01': 'Biomass',
         'B02': 'Lignite',  # 'Fossil Brown coal/Lignite',
         'B03': 'Fossil Coal-derived gas',
         'B04': 'Fossil Gas',
         'B05': 'Fossil Hard coal',
         'B06': 'Fossil Oil',
         'B07': 'Fossil Oil shale',
         'B08': 'Fossil Peat',
         'B09': 'Geothermal',
         'B10': 'Hydro Pumped Storage',
         'B11': 'Hydro Run-of-river and poundage',
         'B12': 'Hydro Water Reservoir',
         'B13': 'Marine',
         'B14': 'Nuclear',
         'B15': 'Other renewable',
         'B16': 'Solar',
         'B17': 'Waste',
         'B18': 'Wind Offshore',
         'B19': 'Wind Onshore',
         'B20': 'Other'}


def namespace(element):
    m = re.match('\{.*\}', element.tag)
    return m.group(0) if m else ''


def attribute(etree_sel):
    return etree_sel.text

@timeit
def import_entsoe_tech(pstart, pend, domains=None, raw=False, entsoe_token=None):
    """
    Importer for the list of installed generators provided by the ENTSO-E
    Trasparency Project. Geographical information is not given.
    If update=True, the dataset is parsed through a request to
    'https://transparency.entsoe.eu/generation/r2/\
    installedCapacityPerProductionUnit/show',
    Internet connection requiered. If raw=True, the same request is done, but
    the unprocessed data is returned.

    Parameters
    ----------
    raw : Boolean, Default False
        Whether to return the raw data, obtained from the request to
        the ENTSO-E transparency platform

    """
    entsoe_token = get_token(entsoe_token)

    # level1 = ['registeredResource.name', 'registeredResource.mRID']
    # level2 = ['voltage_PowerSystemResources.highVoltageLimit', 'psrType']
    # level3 = ['quantity']
    level1 = []
    level2 = ['psrType']
    level3 = ['quantity']

    startyear = pstart[:4]

    cfgfile = dconfig.icap_tech_file
    fname, pth = os.path.basename(cfgfile), os.path.dirname(cfgfile)
    pth = os.path.join(dconfig.download_pth, pth)

    log_cols = ["filename", "BZ", "country", "year", "records", "status"]
    logs = pd.DataFrame(columns = log_cols)

    entsoe = pd.DataFrame()
    for i in domains.index[domains.Country.notnull()]:
        cty = domains.loc[i, 'Country']
        bzf = domains.loc[i, 0]

        llog = [f"{bzf}_{startyear}_{fname}", bzf, cty, startyear]

        msg = "Fetching %s installed generation per type for domain %s (%s)"
        logger.info(msg, startyear, bzf, cty)

        ret = requests.get('https://transparency.entsoe.eu/api',
                           params=dict(securityToken=entsoe_token,
                                       documentType='A68',
                                       processType='A33',
                                       In_Domain=domains.loc[i, 0],
                                       periodStart=pstart,
                                       periodEnd=pend))
        try:
            # Create an ElementTree object
            etree = ET.fromstring(ret.content)
        except ET.ParseError:
            # hack for dealing with unencoded '&' in ENTSOE-API
            etree = ET.fromstring(re.sub(r'&(?=[^;]){6}', r'&amp;',
                                         ret.text).encode('utf-8'))
        ns = namespace(etree)

        status = ret.status_code
        if status not in [200, 500]:
            try:
                status = etree.find(f"{ns}Reason/{ns}text").text
            except AttributeError:
                pass

        df = pd.DataFrame(columns=level1+level2+level3+
                                  ['BZ', 'Country', 'Year'])
        for arg in level1:
            df[arg] = [attribute(e) for e in
                       etree.findall('*/%s%s' % (ns, arg))]
        for arg in level2:
            df[arg] = [attribute(e) for e in
                       etree.findall('*/*/%s%s' % (ns, arg))]
        for arg in level3:
            df[arg] = [attribute(e) for e in
                       etree.findall('*/*/*/%s%s' % (ns, arg))]
        df['BZ'] = domains.loc[i, 0]
        df['Country'] = domains.loc[i, 'Country']
        df['Year'] = startyear
        logger.info("Received data on %d techs", len(df))
        entsoe = entsoe.append(df, ignore_index=True)

        llog += [len(entsoe), status]
        logs.loc[len(logs)] = llog

    if raw:
        return entsoe

    entsoe = (
            entsoe
            .rename(columns={'psrType': 'Fueltype',
                             'quantity': 'Capacity'})
            .replace({'Fueltype': techp})
    )

    entsoe.to_csv(os.path.join(pth, f"{startyear}_{fname}"),
                  index=False, encoding='utf-8')

    logfilename = get_log_filename(pth=pth)
    df = logs
    if os.path.isfile(logfilename):
        df = pd.read_csv(logfilename, header=0).append(logs)
    df.to_csv(logfilename, index=False, encoding='utf-8')

    return entsoe

@timeit
def import_entsoe_gen(pstart, pend, domains=None, raw=False, entsoe_token=None):
    """
    Importer for the hourly generation by production type
    Importer for the list of installed generators provided by the ENTSO-E
    Trasparency Project. Geographical information is not given.
    If update=True, the dataset is parsed through a request to
    'https://transparency.entsoe.eu/generation/r2/\
    installedCapacityPerProductionUnit/show',
    Internet connection requiered. If raw=True, the same request is done, but
    the unprocessed data is returned.

    Parameters
    ----------
    update : Boolean, Default False
        Whether to update the database through a request to the ENTSO-E
        transparency plattform
    raw : Boolean, Default False
        Whether to return the raw data, obtained from the request to
        the ENTSO-E transparency platform

    """
    entsoe_token = get_token(entsoe_token)

    level1 = ['inBiddingZone_Domain.mRID', 'outBiddingZone_Domain.mRID']
    level2 = ['psrType']
    level3 = ['position', 'quantity']

    startyear = pstart[:4]

    cfgfile = dconfig.gen_file
    fname, pth = os.path.basename(cfgfile), os.path.dirname(cfgfile)
    pth = os.path.join(dconfig.download_pth, pth)

    log_cols = ["filename", "BZ", "country", "year", "records", "status"]
    logs = pd.DataFrame(columns = log_cols)

    for i in domains.index[domains.Country.notnull()]:
        entsoe = pd.DataFrame()
        cty = domains.loc[i, 'Country']
        bzf = domains.loc[i, 0]

        llog = [f"{bzf}_{startyear}_{fname}", bzf, cty, startyear]

        logger.info("Fetching %s data for country %s (%s)",
                    startyear,
                    domains.loc[i, 0],
                    domains.loc[i, 'Country'])

        ret = requests.get('https://transparency.entsoe.eu/api',
                           params=dict(securityToken=entsoe_token,
                                       documentType='A75',
                                       processType='A16',
                                       In_Domain=domains.loc[i, 0],
                                       periodStart=pstart,
                                       periodEnd=pend))
        try:
            # Create an ElementTree object
            etree = ET.fromstring(ret.content)
        except ET.ParseError:
            # hack for dealing with unencoded '&' in ENTSOE-API
            etree = ET.fromstring(re.sub(r'&(?=[^;]){6}', r'&amp;',
                                         ret.text).encode('utf-8'))
        ns = namespace(etree)

        status = ret.status_code
        if status not in [200, 500]:
            try:
                status = etree.find(f"{ns}Reason/{ns}text").text
            except AttributeError:
                pass

        recs = []
        for arg in level1[:1]:
            for node in etree.iterfind(f"{ns}TimeSeries/{ns}{arg}/.."):
                bz = attribute(node.find(f"{ns}{arg}"))
                psrType = attribute(node.find(f"*/{ns}psrType"))
                tag_start = f"*/{ns}timeInterval/{ns}start"
                tag_end = f"*/{ns}timeInterval/{ns}end"
                dt_start = attribute(node.find(tag_start))
                dt_end = attribute(node.find(tag_end))
                resolution = attribute(node.find(f"*/{ns}resolution"))
                dt_idx = _get_datetimeindex(dt_start, dt_end, resolution)
                per_points = []
                for p in node.iterfind(f"*/{ns}Point"):
                    pos = attribute(p.find(f"{ns}position"))
                    qty = attribute(p.find(f"{ns}quantity"))
                    per_points.append([bz, psrType, pos, qty])
                try:
                    assert len(per_points) == len(dt_idx)
                except AssertionError:
                    logger.info("Data mismatch in {bzf} for year {startyear}")
                for u, v in zip(per_points[:], dt_idx.tolist()):
                    u.append(v)
                recs += per_points[:]

        df = pd.DataFrame(recs, columns=level1[:1]+level2+level3+["dt"])
        df['Country'] = domains.loc[i, 'Country']
        len_df = len(df)
        if len_df:
            df['dt'] = df['dt'].dt.strftime('%d/%m/%Y %H:%M')
        logger.info("Received %d data points", len_df)

        entsoe = entsoe.append(df, ignore_index=True)

        if raw:
            return entsoe

        entsoe = (entsoe
                  .rename(columns={'psrType': 'Fueltype',
                                   'quantity': 'Generation',
                                   'inBiddingZone_Domain.mRID': 'BZ',
                                   'outBiddingZone_Domain.mRID': 'outBZ'})
                  .replace({'Fueltype': techp})
                  .loc[:, ["dt", "Country", "BZ", "Fueltype", "Generation"]]
        )

        entsoe.to_csv(os.path.join(pth, f"{bzf}_{startyear}_{fname}"),
                      index=False, encoding='utf-8')

        llog += [len(entsoe), status]
        logs.loc[len(logs)] = llog

    logfilename = get_log_filename(pth=pth)
    df = logs
    if os.path.isfile(logfilename):
        df = pd.read_csv(logfilename, header=0).append(logs)
    df.to_csv(logfilename, index=False, encoding='utf-8')

    return entsoe

@timeit
def import_entsoe_wat(pstart, pend, domains=None, raw=False, entsoe_token=None):
    """
    Importer for the hourly generation by production type
    Importer for the list of installed generators provided by the ENTSO-E
    Trasparency Project. Geographical information is not given.
    If update=True, the dataset is parsed through a request to
    'https://transparency.entsoe.eu/generation/r2/\
    waterReservoirsAndHydroStoragePlants/show'
    Internet connection requiered. If raw=True, the same request is done, but
    the unprocessed data is returned.

    Parameters
    ----------
    raw : Boolean, Default False
        Whether to return the raw data, obtained from the request to
        the ENTSO-E transparency platform
    entsoe_token: String
        Security token of the ENTSO-E Transparency platform

    """
    entsoe_token = get_token(entsoe_token)

    level1 = []
    level2 = []
    level3 = ['start', 'quantity']

    startyear = pstart[:4]

    cfgfile = dconfig.wat_file
    fname, pth = os.path.basename(cfgfile), os.path.dirname(cfgfile)
    pth = os.path.join(dconfig.download_pth, pth)

    log_cols = ["filename", "BZ", "country", "year", "records", "status"]
    logs = pd.DataFrame(columns = log_cols)
    for i in domains.index[domains.Country.notnull()]:
        entsoe = pd.DataFrame()
        cty = domains.loc[i, 'Country']
        bzf = domains.loc[i, 0]

        llog = [f"{bzf}_{startyear}_{fname}", bzf, cty, startyear]
        logger.info("Fetching year %s for domain %s (%s)",
                    startyear,
                    domains.loc[i, 0],
                    domains.loc[i, 'Country'])

        ret = requests.get('https://transparency.entsoe.eu/api',
                           params=dict(securityToken=entsoe_token,
                                       documentType='A72',
                                       processType='A16',
                                       In_Domain=domains.loc[i, 0],
                                       periodStart=pstart,
                                       periodEnd=pend))
        try:
            # Create an ElementTree object
            etree = ET.fromstring(ret.content)
        except ET.ParseError:
            # hack for dealing with unencoded '&' in ENTSOE-API
            etree = ET.fromstring(re.sub(r'&(?=[^;]){6}', r'&amp;',
                                         ret.text).encode('utf-8'))
        ns = namespace(etree)

        status = ret.status_code
        if status not in [200, 500]:
            try:
                status = etree.find(f"{ns}Reason/{ns}text").text
            except AttributeError:
                pass

        df = pd.DataFrame(columns=level1 + level2 + level3 +
                                  ['BZ', 'Country', 'Year'])
        for arg in level1:
            df[arg] = [attribute(e) for e in
                       etree.findall('*/%s%s' % (ns, arg))]
        for arg in level2:
            df[arg] = [e.text for e in
                       etree.findall('*/*/%s%s' % (ns, arg))]
        for arg in level3:
            df[arg] = [attribute(e) for e in
                       etree.findall('*/*/*/%s%s' % (ns, arg))]

        df['BZ'] = bzf
        df['Country'] = cty
        df['Year'] = startyear
        len_df = len(df)
        if len_df:
            df['start'] = pd.to_datetime(df['start'])
            df['start'] = df['start'].dt.strftime('%d/%m/%Y %H:%M')
        logger.info("Received %d stored energy values", len_df)
        entsoe = entsoe.append(df, ignore_index=True)

        if raw:
            return entsoe
        entsoe = (entsoe
                  .rename(columns={'start': 'Week starting',
                                   'quantity': 'Stored Energy Value'})
        )

        entsoe.to_csv(os.path.join(pth, f"{bzf}_{startyear}_{fname}"),
                      index=False, encoding='utf-8')

        llog += [len(entsoe), status]
        logs.loc[len(logs)] = llog

    logfilename = get_log_filename(pth=pth)
    df = logs
    if os.path.isfile(logfilename):
        df = pd.read_csv(logfilename, header=0).append(logs)
    df.to_csv(logfilename, index=False, encoding='utf-8')

    return entsoe

@timeit
def import_entsoe_load(pstart, pend, domains=None, raw=False, entsoe_token=None):
    """
    Importer for the hourly generation by production type
    Importer for the list of installed generators provided by the ENTSO-E
    Trasparency Project. Geographical information is not given.
    If update=True, the dataset is parsed through a request to
    'https://transparency.entsoe.eu/generation/r2/\
    waterReservoirsAndHydroStoragePlants/show'
    Internet connection requiered. If raw=True, the same request is done, but
    the unprocessed data is returned.

    Parameters
    ----------
    raw : Boolean, Default False
        Whether to return the raw data, obtained from the request to
        the ENTSO-E transparency platform
    entsoe_token: String
        Security token of the ENTSO-E Transparency platform

    """
    entsoe_token = get_token(entsoe_token)

    level1 = ['outBiddingZone_Domain.mRID']
    level2 = []
    level3 = ['position', 'quantity']

    startyear = pstart[:4]

    cfgfile = dconfig.dem_file
    fname, pth = os.path.basename(cfgfile), os.path.dirname(cfgfile)
    pth = os.path.join(dconfig.download_pth, pth)

    log_cols = ["filename", "BZ", "country", "year", "records", "status"]
    logs = pd.DataFrame(columns = log_cols)
    for i in domains.index[domains.Country.notnull()]:
        entsoe = pd.DataFrame()
        cty = domains.loc[i, 'Country']
        bzf = domains.loc[i, 0]

        llog = [f"{bzf}_{startyear}_{fname}", bzf, cty, startyear]
        logger.info("Fetching year %s for domain %s (%s)",
                    startyear,
                    domains.loc[i, 0],
                    domains.loc[i, 'Country'])

        ret = requests.get('https://transparency.entsoe.eu/api',
                           params=dict(securityToken=entsoe_token,
                                       documentType='A65',
                                       processType='A16',
                                       outBiddingZone_Domain=domains.loc[i, 0],
                                       periodStart=pstart,
                                       periodEnd=pend))
        try:
            # Create an ElementTree object
            etree = ET.fromstring(ret.content)
        except ET.ParseError:
            # hack for dealing with unencoded '&' in ENTSOE-API
            etree = ET.fromstring(re.sub(r'&(?=[^;]){6}', r'&amp;',
                                         ret.text).encode('utf-8'))
        ns = namespace(etree)

        status = ret.status_code
        if status not in [200, 500]:
            try:
                status = etree.find(f"{ns}Reason/{ns}text").text
            except AttributeError:
                pass
        elif status in [200]:
            tag_start = f"{ns}TimeSeries/*/{ns}timeInterval/{ns}start"
            dt_start = etree.find(tag_start).text
            tag_end = f"{ns}TimeSeries/*/{ns}timeInterval/{ns}end"
            dt_end = etree.find(tag_end).text
            resolution = etree.find(f"{ns}TimeSeries/*/{ns}resolution").text

        recs = []
        for arg in level1[:1]:
            for node in etree.iterfind(f"{ns}TimeSeries"):
                bz = attribute(node.find(f"{ns}{arg}"))
                tag_start = f"*/{ns}timeInterval/{ns}start"
                tag_end = f"*/{ns}timeInterval/{ns}end"
                dt_start = attribute(node.find(tag_start))
                dt_end = attribute(node.find(tag_end))
                resolution = attribute(node.find(f"*/{ns}resolution"))
                dt_idx = _get_datetimeindex(dt_start, dt_end, resolution)
                per_points = []
                for p in node.iterfind(f"*/{ns}Point"):
                    pos = attribute(p.find(f"{ns}position"))
                    qty = attribute(p.find(f"{ns}quantity"))
                    per_points.append([bz, pos, qty])
                try:
                    assert len(per_points) == len(dt_idx)
                except AssertionError:
                    logger.info("Data mismatch in {bzf} for year {startyear}")
                for u, v in zip(per_points[:], dt_idx.tolist()):
                    u.append(v)
                recs += per_points[:]

        df = pd.DataFrame(recs, columns=level1+level2+level3+["dt"])
        df['Country'] = domains.loc[i, 'Country']
        len_df = len(df)
        if len_df:
            df['dt'] = df['dt'].dt.strftime('%d/%m/%Y %H:%M')
        logger.info("Received %d actual load values", len_df)

        entsoe = entsoe.append(df, ignore_index=True)

        if raw:
            return entsoe
        entsoe = (entsoe
                  .rename(columns={'start': 'Datetime',
                                   'quantity': 'Actual Total Load'})
        )

        entsoe.to_csv(os.path.join(pth, f"{bzf}_{startyear}_{fname}"),
                      index=False, encoding='utf-8')

        llog += [len(entsoe), status]
        logs.loc[len(logs)] = llog

    logfilename = get_log_filename(pth=pth)
    df = logs
    if os.path.isfile(logfilename):
        df = pd.read_csv(logfilename, header=0).append(logs)
    df.to_csv(logfilename, index=False, encoding='utf-8')

    return entsoe


def _get_datetimeindex(start, end, resolution, tz = None):
    """
    Create a datetimeindex from a parsed tag,
    given that it contains the elements 'start', 'end'
    and 'resolution'

    Parameters
    ----------
    tag : ET tag
    tz: str

    Returns
    -------
    pd.DatetimeIndex
    """
    # start = pd.Timestamp(soup.find('start').text)
    # end = pd.Timestamp(soup.find('end').text)
    if tz is not None:
        start = start.tz_convert(tz)
        end = end.tz_convert(tz)

    # delta = _resolution_to_timedelta(res_text=soup.find('resolution').text)
    delta = _resolution_to_timedelta(res_text=resolution)
    index = pd.date_range(start=start, end=end, freq=delta, closed='left')
    if tz is not None:
        dst_jump = len(set(index.map(lambda d:d.dst()))) > 1
        if dst_jump and delta == "7D":
            # For a weekly granularity, if we jump over the DST date in October,
            # date_range erronously returns an additional index element
            # because that week contains 169 hours instead of 168.
            index = index[:-1]
        index = index.tz_convert("UTC")

    return index


def _resolution_to_timedelta(res_text: str) -> str:
    """
    Convert an Entsoe resolution to something that pandas can understand
    """
    resolutions = {
        'PT60M': '60min',
        'P1Y': '12M',
        'PT15M': '15min',
        'PT30M': '30min',
        'P7D': '7D',
        'P1M': '1M',
    }
    delta = resolutions.get(res_text)
    if delta is None:
        raise NotImplementedError("Sorry, I don't know what to do with the "
                                  "resolution '{}', because there was no "
                                  "documentation to be found of this format. "
                                  "Everything is hard coded. Please open an "
                                  "issue.".format(res_text))
    return delta


def get_token(entsoe_token):
    if entsoe_token is None:
        entsoe_token = os.getenv("entsoe_token")
    assert entsoe_token is not None, "entsoe_token is missing"
    return entsoe_token


def get_geo_domains(data=None):
    if data is None:
        data = _data('in/entsoe-cty.csv')
    return (pd.read_csv(data, sep=',', header=None)
            .rename(columns={1: "Country"}))


def get_log_filename(pth=None):
    if pth is None:
        pth = os.getcwd()
    fldr = os.path.basename(pth)
    return os.path.join(pth, f"{fldr}.csv")


def update_logs(drun, pth=None):
    if pth is None:
        pth = os.getcwd()
    fldr = os.path.basename(pth)
    logfile = os.path.join(pth, f"{fldr}.csv")
    if os.path.isfile(logfile):
        df = pd.read_csv(logfile, header=0)
        dct = dict(zip(df["filename"], df["status"]))
    else:
        dct = dict()

    dct.update()

    return dct

@timeit
def get_transparency_techs(dconfig):
    pth_data = os.path.join(dconfig.download_pth, "icap", "20*.csv")
    pth_dataset = dconfig.dataset_pth

    map_Fueltype_Type = pd.read_csv(_data_maps("Fueltype_Type.csv"))
    techs = dict(zip(map_Fueltype_Type.Fueltype, map_Fueltype_Type.Type))

    colrenames = {"Country": "zone", "Fueltype": "type", "Capacity": "icap",
                  "Year": "year"}

    ddf = dd.read_csv(pth_data, header=0)
    df = (ddf.compute()
          .assign(Fueltype=lambda x: x.Fueltype.map(techs),
                  Country=lambda x: x.Country.str.strip(),
                  generator=lambda x: x["Country"].str.cat(x["Fueltype"],
                                                           sep="-"))
          .rename(columns=colrenames)
          )
    cols = ["zone", "type", "generator", "icap", "year"]
    df = df.loc[:, cols]
    df.to_csv(os.path.join(pth_dataset,"generator_icap_actual.csv"),
                           index=False)


def map_countries():
    """

    :return: dictionary
    """
    pth_ctys = os.path.join(os.path.dirname(__file__), "data", "in")
    map_zone_country = pd.read_csv(os.path.join(pth_ctys, "entsoe-cty.csv"),
                                   names=["BZ", "country"])
    return dict(zip(map_zone_country.BZ, map_zone_country.country))


def map_techs():
    """

    :return: dictionary
    """
    map_Fueltype_Type = pd.read_csv(_data_maps("Fueltype_Type.csv"))
    return dict(zip(map_Fueltype_Type.Fueltype, map_Fueltype_Type.Type))


@timeit
def stash_hourly_generation(dconfig):
    """

    :return: None
    """
    pth_data = os.path.join(dconfig.download_pth, "hourly_generation")
    techs = map_techs()

    def stash_file(f):
        """

        :param f: str of full filename
        :return: None
        """
        pth_stash = os.path.join(dconfig.stash_pth, "hourly_generation")
        cols = {"dt": "datetime", "Generation": "value"}
        df = (pd.read_csv(f, header=0, encoding="utf-8")
              .drop(columns=["BZ"])
              .assign(Fueltype=lambda x: x.Fueltype.map(techs),
                      name=lambda x:
              x["Country"].str.strip().str.cat(x["Fueltype"], sep="-"))
              .rename(columns=cols)
              .loc[: , ["datetime", "name", "value"]]
              .set_index("datetime")
              )
        df.index = pd.to_datetime(df.index, format="%d/%m/%Y %H:%M")
        df = (df
              .groupby("name")
              .resample("H").mean()
              )
        if len(df):
            df.to_csv(os.path.join(pth_stash, os.path.basename(f)), index=True)

    fs = glob.glob(f"{pth_data}/10*.csv")
    list(map(stash_file, fs))

@timeit
def stash_hourly_load(dconfig):
    """

    :return: None
    """
    pth_data = os.path.join(dconfig.download_pth, "hourly_load")
    ctys = map_countries()

    def stash_file(f):
        """

        :param f: str of full filename
        :return: None
        """
        pth_stash = os.path.join(dconfig.stash_pth, "hourly_load")
        cols = {"dt": "datetime", "outBiddingZone_Domain.mRID": "BZ",
                "Actual Total Load": "value"}
        df = (pd.read_csv(f, header=0, encoding="utf-8")
              .rename(columns=cols)
              .assign(name=lambda x: x.BZ.map(ctys))
              .drop(columns=["BZ"])
              .loc[:, ["name", "datetime", "value"]]
              .set_index("datetime")
              )
        df.index = pd.to_datetime(df.index)
        df = (df
              .groupby("name")
              .resample("H").mean()
              )
        if len(df):
            df.to_csv(os.path.join(pth_stash, os.path.basename(f)), index=True)

    fs = glob.glob(f"{pth_data}/10*.csv")
    list(map(stash_file, fs))

@timeit
def download_data(dconfig):
    try:
        DOMAINS = StringIO(dconfig.GEODOMAIN)
    except:
        DOMAINS = None
    domains = get_geo_domains(DOMAINS)
    timerefs = dconfig.TIMEDOMAINS

    for k, (pstart, pend) in timerefs.items():
        import_entsoe_gen(pstart, pend,  domains=domains, raw=False)
        import_entsoe_tech(pstart, pend, domains=domains, raw=False)
        import_entsoe_wat(pstart, pend, domains=domains, raw=False)
        import_entsoe_load(pstart, pend, domains=domains, raw=False)

@timeit
def main(dconfig, update=False):
    if update:
        download_data(dconfig)
    get_transparency_techs(dconfig)
    stash_hourly_generation(dconfig)
    stash_hourly_load(dconfig)


if __name__ == "__main__":
    load_dotenv(find_dotenv(usecwd=True), verbose=True)
    args = get_args()
    dconfig, update = get_data_config(args.config), args.update
    main(dconfig, update)
