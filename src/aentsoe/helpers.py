"""Miscellaneous helper functions"""
from os.path import dirname
import os
import logging

import datetime
import time
from functools import wraps
from typing import Any, Callable

logging.basicConfig(level=logging.INFO)

def timeit(func: Callable[..., Any]) -> Callable[..., Any]:
    """Times a function, usually used as decorator"""

    @wraps(func)
    def timed_func(*args: Any, **kwargs: Any) -> Any:
        """Returns the timed function"""
        start_time = time.time()
        result = func(*args, **kwargs)
        elapsed_time = datetime.timedelta(seconds=(time.time() - start_time))
        print(f"{func.__name__:40} completed in {elapsed_time}")
        return result

    return timed_func


def _data_maps(fn):
    return os.path.join(dirname(__file__), 'data', 'maps', fn)


def _data(fn):
    return os.path.join(dirname(__file__), 'data', fn)


def _data_in(fn):
    return os.path.join(dirname(__file__), 'data', 'in', fn)


def _data_out(fn, config=None):
    if config is None:
        return os.path.join(dirname(__file__), '..', '..','data', 'out',
                            'default', fn)
    else:
        return os.path.join(dirname(__file__), '..', '..','data', 'out',
                            config['hash'], fn)
