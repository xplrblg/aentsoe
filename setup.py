"""A setuptools based setup module.
See:
https://packaging.python.org/guides/distributing-packages-using-setuptools/
https://github.com/pypa/sampleproject
https://stackoverflow.com/questions/19602582/pip-install-editable-links-to-wrong-path
"""
from setuptools import setup

reqs = ["python-dotenv",
        "dask[complete]",
        "numpy",
        "pandas",
        "xlrd",
        "ruamel.yaml",
        "requests"]

setup(
    name='aentsoe',
    version='0.0.1',
    author='qheuristics',
    author_email='qheuristics@gmail.com',
    description='Data prep of entsoe sourced info',
    python_requires='>=3.5, <4',
    package_dir={'': 'src'},
    packages=['aentsoe'],
    package_data={'aentsoe': ['data\\maps\\*.csv'] + ['data\\in\\*.csv']
                             + ['data\\*.csv']},
    install_requires=reqs,
)
